'use strict';

/* Controllers */

angular.module('rest-mood-app')
.controller('HomeCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', 'MoodData', function($rootScope, $scope, $location, $localStorage, Main, MoodData) {

	// user token
	$scope.token = $localStorage.token;

	// get values injected on service
	$scope.mood = Main.mood;
	$scope.bgCol = Main.bgCol;
	$scope.user = undefined;
	$scope.neFrac = Main.neFrac;
	$scope.nwFrac = Main.nwFrac;
	$scope.seFrac = Main.seFrac;
	$scope.swFrac = Main.swFrac;
	$scope.currentYear = new Date().getFullYear();
	$scope.nelist = [];
	$scope.nwlist = [];
	$scope.selist = [];
	$scope.swlist = [];
	$scope.dashquery = [];
	$scope.yearnelist = [];
	$scope.yearnwlist = [];
	$scope.yearselist = [];
	$scope.yearswlist = [];
	$scope.moodChartLabels = ["High Energy and Good", "High Energy and Bad", "Low Energy and Good", "Low Energy and Bad"];
	$scope.moodChartData = [25,25,25,25];
	$scope.moodChartcolors = ['#FACC2E','#DF0101','#31B404','#58ACFA'];
	$scope.lastFive = [
	                   { name: '', tstamp: 0},
	                   { name: '', tstamp: 0},
	                   { name: '', tstamp: 0},
	                   { name: '', tstamp: 0},
	                   { name: '', tstamp: 0}
	                   ];

	//TEXTAREA
	//::::::::::::::::::::::::::::::::::::::::::::::::::::
	//start
	$scope.commentLength = 120;
	$scope.$watch('comment.Comment.body', function (newValue, oldValue) {
		if (newValue) {
			if (newValue.length > 120) {
				$scope.comment.Comment.body = oldValue;
			}
			$scope.commentLength = 120 - newValue.length;
		}
	});

	$scope.updateBody = function (event) {
		console.log(event);
		return false;
	};
	//end

	//REPORTS TABLE
	//:::::::::::::::::::::::::::::::::::::::::::::::::::::
	$scope.sort = function(keyname){
		$scope.sortKey = keyname;   //set the sortKey to the param passed
		$scope.reverse = !$scope.reverse; //if true make it false and vice versa
	};

	//CHARTS
	//::::::::::::::::::::::::::::::::::::::::::::::::::::
	//chart: line
	//start
	var months = [
	              { name: 'January', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'February', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'March', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'April', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'May', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'June', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'July', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'August', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'September', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'October', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'November', ne: 0, nw: 0, se: 0, sw: 0 },
	              { name: 'December', ne: 0, nw: 0, se: 0, sw: 0 }
	              ];
	$scope.linelabels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	$scope.lineseries = ['High Energy and Good', 'High Energy and Bad', 'Low Energy and Good', 'Low Energy and Bad'];
	$scope.linedataX = [[0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0]];
	$scope.getNElist = function() {
		for(var i=0; i< months.length; i++){
			$scope.nelist[i]=months[i].ne;
		}
		for(var i=0; i< months.length; i++){
			$scope.nwlist[i]=months[i].nw;
		}
		for(var i=0; i< months.length; i++){
			$scope.selist[i]=months[i].se;
		}
		for(var i=0; i< months.length; i++){
			$scope.swlist[i]=months[i].sw;
		}
		$scope.linedataX[0]=$scope.nelist;
		$scope.linedataX[1]=$scope.nwlist;
		$scope.linedataX[2]=$scope.selist;
		$scope.linedataX[3]=$scope.swlist;
	};


	$scope.updateMonthsSeriesData = function() {
		var list = $scope.dashquery;
		var neCount=0;
		var nwCount=0;
		var seCount=0;
		var swCount=0;
		for(var i=0; i< months.length; i++){
			for(var j=0; j< list.length; j++){
				if(i==(list[j].tstamp.split("-")[1].replace(/^[0]+/g,"")-1)){
					if(list[j].quadrant == 'ne'){
						neCount += list[j].qty;
						months[i].ne=neCount;
					}else if(list[j].quadrant == 'nw'){
						nwCount += list[j].qty;
						months[i].nw=nwCount;
					}else if(list[j].quadrant == 'se'){
						seCount += list[j].qty;
						months[i].se=seCount;
					}else if(list[j].quadrant == 'sw'){
						swCount += list[j].qty;
						months[i].sw=swCount;
					}
				}
			}
			neCount=0;
			nwCount=0;
			seCount=0;
			swCount=0;
		}
		$scope.getNElist();
		//return months;
	};
	//end: chart line

	//::::::::::::::::::::::::::::::::::::::::::::::::::::
	//chart: dohnut
	//start
	$scope.getFrac = function(quadrant) {
		var neCount = 0;
		var total = 0;
		var list = $scope.dashquery;
		for(var i=0; i< list.length; i++){
			if(list[i].quadrant == quadrant){
				neCount += list[i].qty;
			}
			total += list[i].qty;
		}
		if(list.length){
			Main.neFrac = neCount/total;
		}
		return (Main.neFrac*100).toFixed(2);
	};

	$scope.loadMoodsChart = function() {
		$scope.moodChartData[0] = $scope.getFrac('ne');
		$scope.moodChartData[1]	= $scope.getFrac('nw');
		$scope.moodChartData[2]	= $scope.getFrac('se');
		$scope.moodChartData[3]	= $scope.getFrac('sw');
	};
	//end: dohnut chart


	//::::::::::::::::::::::::::::::::::::::::::::::::::::
	//chart: bars
	//start
	var years = [
	             { name: '2013', ne: 0, nw: 0, se: 0, sw: 0 },
	             { name: '2014', ne: 0, nw: 0, se: 0, sw: 0 },
	             { name: '2015', ne: 0, nw: 0, se: 0, sw: 0 }
	             ];
	$scope.yearlabels = ['2013', '2014', '2015'];
	$scope.yearseries = ['High Energy and Good', 'High Energy and Bad', 'Low Energy and Good', 'Low Energy and Bad'];
	$scope.yeardata = [[0,0,0],[0,0,0],[0,0,0],[0,0,0]];

	$scope.getYearsList = function() {
		for(var i=0; i< years.length; i++){
			$scope.yearnelist[i]=years[i].ne;
		}
		for(var i=0; i< years.length; i++){
			$scope.yearnwlist[i]=years[i].nw;
		}
		for(var i=0; i< years.length; i++){
			$scope.yearselist[i]=years[i].se;
		}
		for(var i=0; i< years.length; i++){
			$scope.yearswlist[i]=years[i].sw;
		}
		$scope.yeardata[0]=$scope.yearnelist;
		$scope.yeardata[1]=$scope.yearnwlist;
		$scope.yeardata[2]=$scope.yearselist;
		$scope.yeardata[3]=$scope.yearswlist;
	};

	$scope.updateYearsSeriesData = function() {
		var list = $scope.dashquery;
		var neCount=0;
		var nwCount=0;
		var seCount=0;
		var swCount=0;
		for(var i=0; i< years.length; i++){
			for(var j=0; j< list.length; j++){
				if(years[i].name==list[j].tstamp.split("-")[0]){
					if(list[j].quadrant == 'ne'){
						neCount += list[j].qty;
						years[i].ne=neCount;
					}else if(list[j].quadrant == 'nw'){
						nwCount += list[j].qty;
						years[i].nw=nwCount;
					}else if(list[j].quadrant == 'se'){
						seCount += list[j].qty;
						years[i].se=seCount;
					}else if(list[j].quadrant == 'sw'){
						swCount += list[j].qty;
						years[i].sw=swCount;
					}
				}
			}
			neCount=0;
			nwCount=0;
			seCount=0;
			swCount=0;
		}
		$scope.getYearsList();
	};
	//end: bars chart

	//::::::::::::::::::::::::::::::::::::::::::::::::::::
	//chart: radar
	//start

	$scope.radarlabels =["Mood", "Mood", "Mood", "Mood", "Mood", "Mood", "Mood", "Mood", "Mood", "Mood"];
	$scope.radardata = [[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0]];
	$scope.radarseries = ['Top 10 Mood', 'Top 1 Mood'];

	$scope.updateRadarSeriesData = function() {
		var list = $scope.dashquery;
		for(var i=0; i<list.length; i++){
			$scope.radarlabels[i]=list[i].name;
			$scope.radardata[0][i]=list[i].qty;
			if (i==9){ break; }
		}
		for(var i=0; i<10; i++){
			if(list.length>0){
				$scope.radardata[1][i]=list[0].qty;
			}else{
				$scope.radardata[1][i]=0;
			}
		}
		//return $scope.radardata;
	};
	//end: radar chart


	//GENERAL
	//::::::::::::::::::::::::::::::::::::::::::::::::::::
	$scope.selectMood = function() {
		if ($scope.mood) {
			Main.bgCol = MoodData.getColor($scope.mood.valence, $scope.mood.arousal);
			Main.mood = $scope.mood;
			$location.path('mood');
		}
	};

	$scope.checkSelectedMood = function() {
		if (!$scope.mood) {
			$location.path('matrix');
		}
	};

	$scope.getLastFive = function() {
		var qty=0;
		var list = $scope.getMoodsList();
		for(var i=list.length; i>0; i--){
			$scope.lastFive[qty].name=list[i-1].name;
			$scope.lastFive[qty++].tstamp=list[i-1].tstamp.split(".")[0].replace('T',' ');
			if(5==qty) break;
		}
		return $scope.lastFive;
	};

	$scope.getMoodsList = function() {
		return $scope.dashquery;
	};

	$scope.getReportsList = function() {
		$scope.rList = Main.reportsList;
	};

	$scope.listMood = function() {
		// set values on service
		Main.listmood($scope.mood, function(res) {
			$scope.rList = res;

			//redirect to report
			$location.path('description');
		}, function() {
			$rootScope.error = 'Failure when attempt to access';
		});
	};

	$scope.showReports = function() {
		// set values on service
		Main.showreports(function(res) {
			$scope.dashquery = res;

			//redirect to report
			$location.path('reports');
		}, function() {
			$rootScope.error = 'Failure when attempt to access';
		});
	};

	$scope.saveMood = function() {
		// set values on service
		if($scope.comment) {
			$scope.mood.description = $scope.comment.Comment.body;
		} else {
			$scope.mood.description = "";
		}

		Main.savemood($scope.mood, function(res) {
			Main.moodsList = res;
			//redirect to report
			$location.path('reports');
		}, function() {
			$rootScope.error = 'Failure when attempt to access';
		});
	};

	$scope.signin = function() {
		console.log(Main);
		console.log($scope.user);
		Main.signin($scope.user, function(res) {
			console.log("RES DATA "+ JSON.stringify(res));

			$localStorage.token = res.token;
			$scope.token = $localStorage.token;
			$location.path('matrix');



		}, function() {
			$rootScope.error = 'Failure when attempt to access';
		});
	};

	$scope.signup = function() {
		var formData = {
				email: $scope.email,
				password: $scope.password
		};

		Main.save(formData, function(res) {
			if (res.type == false) {
				alert(res.data);
			} else {
				$localStorage.token = res.data.token;
				window.location = "/";
			}
		}, function() {
			$rootScope.error = 'Failed to register';
		});
	};

	$scope.me = function() {
		Main.me(function(res) {
			$scope.myDetails = res;
		}, function() {
			$rootScope.error = 'Failed to fetch the data';
		});
	};

	$scope.logout = function() {
		Main.logout(function() {
			$localStorage.token = undefined;
			window.location = "/";
		}, function() {
			console.log("Failed to log out");
		});
	};
}]);
