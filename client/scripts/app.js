/* Application File */

angular.module('rest-mood-app', [
  'ngStorage',
  'ngRoute',
  'angular-mood',
  'chart.js',
  'angularUtils.directives.dirPagination'
])
.config(['$routeProvider', '$httpProvider', 'ChartJsProvider', function ($routeProvider, $httpProvider, ChartJsProvider) {
    //ngRoute
    $routeProvider.
        when('/', {
            templateUrl: 'partials/login.html',
            controller: 'HomeCtrl'
        }).
        when('/matrix', {
            templateUrl: 'partials/matrix.html',
            controller: 'HomeCtrl'
        }).
        when('/reports', {
            templateUrl: 'partials/reports.html',
            controller: 'HomeCtrl'
        }).
        when('/mood', {
            templateUrl: 'partials/mood.html',
            controller: 'HomeCtrl'
        }).
        when('/description', {
            templateUrl: 'partials/description.html',
            controller: 'HomeCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
    //angular
	$httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage) {
        return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = $localStorage.token;
                    } else {
                      if($location.$$url !== '/') {
                          $location.path('/');
                      }

                    }
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        $location.path('/');
                    }
                    return $q.reject(response);
                }
            };
        }]);

			}]);
