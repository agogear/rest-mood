'use strict';

/* Services */

angular.module('rest-mood-app')
.factory('Main', ['$http', '$localStorage', function($http, $localStorage){
	var baseUrl = "http://localhost:3000";


	// auxiliar variables
	var moodSelected = undefined;
	var bgCol = undefined;
	var moodsList = undefined;
	var reportsList = undefined;
	var showReportsList = undefined;
	var neFrac = undefined;
	var nwFrac = undefined;
	var seFrac = undefined;
	var swFrac = undefined;

	var token = undefined;

	return {
		save: function(data, success, error) {
			$http.post(baseUrl + '/signin', data).success(success).error(error);
		},
		signin: function(data, success, error) {
			$http.post(baseUrl + '/authenticate', data).success(success).error(error);
		},
		me: function(success, error) {
			$http.get(baseUrl + '/me').success(success).error(error);
		},
		logout: function(success) {
			delete $localStorage.token;
			success();
		},
		savemood: function(data, success, error) {
			$http.post(baseUrl + '/save', data).success(success).error(error);
		},
		listmood: function(data, success, error) {
			$http.post(baseUrl + '/list', data).success(success).error(error);
		},
		showreports: function(success, error) {
			$http.get(baseUrl + '/showreport').success(success).error(error);
		}
	};
}
]);
