// config/database.js
module.exports = {
    'connection': {
        'host': 'localhost',
        'user': 'dbuser',
        'password': '123456',
        'port' : 8889,
        'time_zone': 'Australia/Sydney'
    },
	'database': 'mood_db'
};
