//server.js

//set up ======================================================================
//get all the tools we need
var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator'),
    morgan = require('morgan');
var port   = process.env.PORT || 3000;

//set up express application
app.use('/', express.static(path.join(__dirname, '../client')));//static app
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());
app.use(morgan('dev')); // log every request to the console
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
  next();
});

//routes ======================================================================
require('./routes/routes.js')(app); // load routes and pass in the app
// launch ======================================================================
app.listen(port);
console.log('Woohoo! Are you ready for the magic? It happens on port ' + port);
