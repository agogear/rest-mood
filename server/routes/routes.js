//routes/routes.js

//load up the user model
var mysql = require('mysql');
var jwt = require("json-web-token");
var dbconfig = require('../config/database');
var connection = mysql.createConnection(dbconfig.connection);
connection.query('USE ' + dbconfig.database);
//connection.query("SET @@session.time_zone='Australia/Sydney'");
module.exports = function(app) {

	// :::::::::::::::::::::::::::::::::::::
	// MATRIX ==============================
	// :::::::::::::::::::::::::::::::::::::
	// show the matrix page
	app.get('/matrix', function(req, res) {
		console.log('::::::::::::MOOD PAGE:::::::::::::::::');
		// render the page
		res.render('matrix.ejs');
	});

	app.post('/authenticate', checkUser, function(req, res) {
		console.log('::::::::::::: AUTHENTICATE ::::::::::::::::');
		// render the page
		res.json(req.body);
	});

	// :::::::::::::::::::::::::::::::::::::
	// SAVE MOOD ===========================
	// :::::::::::::::::::::::::::::::::::::
	// save mood after user confirmation
	app.post('/save', function(req, res) {
		console.log('::::::::::SAVE/LIST MOOD::::::::::::::');
		connection.query("INSERT INTO " +
				"t_mood (userid, name, valence, arousal, quadrant, description ) " +
				"values " +
				"((SELECT userid FROM t_user " +
				"WHERE " +
				"token='"+req.headers.authorization+"'),'"+req.body.name+"',"+req.body.valence+","+req.body.arousal+",'"+getQuadrant(req.body.valence,req.body.arousal)+"', '"+req.body.description+"')", 
				function(err, data) {
			if (err) {
				console.error(err);
				res.statusCode = 500;
				res.send({
					result: 'error',
					err:    err.code
				});
			}
		});
		connection.query("SELECT name, valence, arousal, quadrant, tstamp, COUNT(*) as qty " +
				"FROM t_mood " +
				"WHERE userid=" +
				"(SELECT userid FROM t_user " +
				"WHERE " +
				"token='"+req.headers.authorization+"') " +
				"GROUP BY name " +
				"ORDER BY qty DESC", function(err, data) {
			if (err) {
				console.error(err);
				res.statusCode = 500;
				res.send({
					result: 'error',
					err:    err.code
				});
			}
			res.json(data);
		});
	});

	
	// :::::::::::::::::::::::::::::::::::::
	// REPORTS (SCREEN) ====================
	// :::::::::::::::::::::::::::::::::::::
	// show reports screen (from matrix screen)
	app.get('/showreport', function(req, res) {
		console.log('::::::::::REPORTS::::::::::::::');
		connection.query("SELECT name, valence, arousal, quadrant, tstamp, COUNT(*) as qty " +
				"FROM t_mood " +
				"WHERE userid=" +
				"(SELECT userid FROM t_user " +
				"WHERE " +
				"token='"+req.headers.authorization+"') " +
				"GROUP BY name " +
				"ORDER BY qty DESC", function(err, data) {
			if (err) {
				console.error(err);
				res.statusCode = 500;
				res.send({
					result: 'error',
					err:    err.code
				});
			}
			res.json(data);
		});
	});
	
	
	// :::::::::::::::::::::::::::::::::::::
	// TABLE (DESCRIPTION) =================
	// :::::::::::::::::::::::::::::::::::::
	// list mood description
	app.post('/list', function(req, res) {
		console.log('::::::::::LIST(DESC) MOOD::::::::::::::');
		connection.query("SELECT name, description, tstamp " +
				"FROM t_mood " +
				"WHERE userid=" +
				"(SELECT userid FROM t_user " +
				"WHERE " +
				"token='"+req.headers.authorization+"') ORDER BY tstamp DESC", 
				function(err, data) {
			if (err) {
				console.error(err);
				res.statusCode = 500;
				res.send({
					result: 'error',
					err:    err.code
				});
			}
			res.json(data);
		});
	});

};

//::::::::::::::::::::::::::::::::::
//authentication functions =========
//::::::::::::::::::::::::::::::::::
function checkUser(req, res, next){
	console.log(':::::::::::::Checking if user exists in the system:::::::::::::::::');
	connection.query('SELECT token FROM t_user WHERE userid=' + req.body.id, function(err, data) {
		if (err) {
			console.error(err);
			res.statusCode = 500;
			res.send({
				result: 'error',
				err:    err.code
			});
		}else{
			req.body.token = jwt.encode("MY_APP_SECRET_KEY", req.body).value;
			if(!data.length){
				console.log('>>creating authorization token for new user');
				addUser(req,res);
				next();
			}else{
				if(req.body.token!=data[0].token){
					console.log(">>invalid token for user");
					res.statusCode = 401;
					res.send({
						result: 'error'
					});
				}else{
					next();
				}
			}
		}
	});

	function addUser(req, res){
		console.log(':::::::::::::Adding user to the system:::::::::::::::::');
		var insertQuery = "INSERT INTO " +
		"t_user ( token, userid ) values (?,?)";
		connection.query(insertQuery,[req.body.token, req.body.id], function(err, data) {
			if (err) {
				console.error(err);
				res.statusCode = 500;
				res.send({
					result: 'error',
					err:    err.code
				});
			}
		});
	}
}

function getQuadrant(valence, arousal) {
	if (valence >= 0)
		if (arousal >= 0)
			return "ne" ;
		else
			return "se" ;
	else
		if (arousal >= 0)
			return "nw" ;
		else
			return "sw" ;
}
