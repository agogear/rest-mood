# *How's your mood?*

## What's in here?
A web application for recording mood, and tracking how it changes. This web app makes use of [angular-mood](https://github.com/dnmilne/angular-mood) widget.
Each user is able to record their current mood, and to attach a description of what made them feel that way. 

## Key features

This app has two separate components:
* A nicely documented API that conforms to REST best practices, developed using NodeJS. 
* Access to a persistent database of mood recordings (mysql)
* A web application developed in Angular that provides a user-friendly interface to this API, for recording moods and viewing recently recorded moods.

Additionally
* Allow users to (optionally) authenticate via Facebook
* Provide a nice visualization of how the user's mood has changed over time


## Installation
* Server component (/server):
Install with npm
```shell
npm install
```

* Client component (/client):
Install with bower 
```shell
bower install
```

## Configuration (database)
Please update 'server/config/database.js' file.
DB script available in 't_user.sql' file.

## Navigational flow
* PDF Document [here](https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnxlYW91ZnBlfGd4OmIyMzBjNWQ1MmZjMmU4MQ)

![angular-mood-project.001.png](https://bitbucket.org/repo/MM6pra/images/26397938-angular-mood-project.001.png)
![angular-mood-project.002.png](https://bitbucket.org/repo/MM6pra/images/555780911-angular-mood-project.002.png)
![angular-mood-project.003.png](https://bitbucket.org/repo/MM6pra/images/531207169-angular-mood-project.003.png)
![angular-mood-project.004.png](https://bitbucket.org/repo/MM6pra/images/3265903721-angular-mood-project.004.png)
![angular-mood-project.005.png](https://bitbucket.org/repo/MM6pra/images/832729184-angular-mood-project.005.png)
![angular-mood-project.006.png](https://bitbucket.org/repo/MM6pra/images/1604738138-angular-mood-project.006.png)
![angular-mood-project.007.png](https://bitbucket.org/repo/MM6pra/images/2275294083-angular-mood-project.007.png)
![angular-mood-project.008.png](https://bitbucket.org/repo/MM6pra/images/2356008352-angular-mood-project.008.png)
![angular-mood-project.009.png](https://bitbucket.org/repo/MM6pra/images/3139459617-angular-mood-project.009.png)
![angular-mood-project.010.png](https://bitbucket.org/repo/MM6pra/images/1037994671-angular-mood-project.010.png)
![angular-mood-project.011.png](https://bitbucket.org/repo/MM6pra/images/3947463722-angular-mood-project.011.png)
![angular-mood-project.012.png](https://bitbucket.org/repo/MM6pra/images/1212968747-angular-mood-project.012.png)

## Demonstration
* [Youtube video](https://www.youtube.com/watch?v=3BhaCv4wvHI)

Please contact me if you have any queries.

Kind Regards, 

Eduardo Oliveira.

[site](http://www.eduoliveira.com) ||  [linkedin](https://au.linkedin.com/in/oliveirae) || 
email: eduardo.oliveira[at]unimelb.edu.au